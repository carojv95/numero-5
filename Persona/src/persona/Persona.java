/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persona;

/**
 *
 * @author admin
 */
public class Persona {

  
    protected String nombre;
    protected String apellido;
    protected String ciudad_nacimiento;

    public Persona(String nombre, String apellido, String ciudad_nacimiento) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.ciudad_nacimiento = ciudad_nacimiento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCiudad_nacimiento() {
        return ciudad_nacimiento;
    }

    public void setCiudad_nacimiento(String ciudad_nacimiento) {
        this.ciudad_nacimiento = ciudad_nacimiento;
    }
    
}
