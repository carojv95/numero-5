/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persona;


/**
 *
 * @author admin
 */
public class Alumno extends Persona {
    protected int numero_materias;

    public Alumno(int numero_materias, String nombre, String apellido, String ciudad_nacimiento) {
        super(nombre, apellido, ciudad_nacimiento);
        this.numero_materias = numero_materias;
    }

    public int getNumero_materias() {
        return numero_materias;
    }

    public void setNumero_materias(int numero_materias) {
        this.numero_materias = numero_materias;
    }
    
    
    
     @Override
     public String toString(){
         
        String a = ("Nombre: "+getNombre()+"\nApellido: "+getApellido()+"\nCiudad: "+getCiudad_nacimiento());

        return a;
    }

  
}