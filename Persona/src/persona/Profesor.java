/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persona;

/**
 *
 * @author admin
 */
public class Profesor extends Persona {

    protected int numero_horas;
    protected String profesion;

    public Profesor(int numero_horas, String profesion, String nombre, String apellido, String ciudad_nacimiento) {
        super(nombre, apellido, ciudad_nacimiento);
        this.numero_horas = numero_horas;
        this.profesion = profesion;
    }

  

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public int getNumero_horas() {
        return numero_horas;
    }

    public void setNumero_horas(int numero_horas) {
        this.numero_horas = numero_horas;
    }

    @Override
    public String toString() {
        String a = ("Nombre: " + getNombre() + "\nApellido: " + getApellido() + "\nCiudad: " + getCiudad_nacimiento() + "\nProfesion: "+ getProfesion());
        return a;

    }
}
